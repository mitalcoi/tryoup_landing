import ScrollListener from './scrollListener';
import {isMobile} from './tools';

let pageIndex = 0;

export default (index = 0) => {
    pageIndex = index;
    const scrollListener = window.scrollListener;

    if (!isMobile.any()) {
        scrollListener.onMove((event, data) => {
            if (data.nextIndex == pageIndex) {
                $('.performance-screen').find('.description').removeClass('hide').addClass('show');
                $('.performance-screen').find('.performance-grid').removeClass('hide').addClass('show');
            }
    
            if (data.nextIndex == pageIndex - 1) {
                $('.performance-screen').find('.description').removeClass('show').addClass('hide');
                $('.performance-screen').find('.performance-grid').removeClass('show').addClass('hide');
            }
        });
    } else {
        $('.performance-screen').find('.description').removeClass('hide').addClass('show');
        $('.performance-screen').find('.performance-grid').removeClass('hide').addClass('show');
    }
};