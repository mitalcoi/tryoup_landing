import Snap from 'snapsvg';
import ScrollListener from './scrollListener';
import SnapContainer from './snapContainer';
import {isMobile} from './tools';

let snapContainer = null;
let pageIndex = 0;
let screenIsReady = false;

export default (index = 0) => {
    pageIndex = index;
    const containerID = '#flowContainer';
    const snapContainerID = '#flowSnapContainer';
    const svgName = 'product-scheme.svg';
    const mobSvgName = 'product-scheme-mob.svg';
    const scrollListener = window.scrollListener;

    const fileName = isMobile.any() ? mobSvgName : svgName;
    snapContainer = new SnapContainer(containerID, snapContainerID, fileName, SVGReadyHandler);

    if (isMobile.any()) {
        $('.product-screen').removeClass('hide').addClass('show');
        $('.description').removeClass('hide').addClass('show');
        snapContainer.snap.removeClass('hide').addClass('show');
    } else {
        scrollListener.onMove((event, data) => {
            if (data.nextIndex == pageIndex) {
                $('.product-screen').removeClass('hide').addClass('show');
                $('.description').removeClass('hide').addClass('show');
                snapContainer.snap.removeClass('hide').addClass('show');
                ShowBlocks();
            }
    
            if (data.nextIndex == pageIndex - 1) {
                $('.product-screen').removeClass('show').addClass('hide');
                $('.description').addClass('hide').removeClass('show');
                snapContainer.snap.addClass('hide').removeClass('show');
            }
        });
    }
};

let secondVisibilityGroup = null;
let thirdVisibilityGroup = null;
let fourthVisibilityGroup = null;

const SVGReadyHandler = () => {
    if (isMobile.any()) {
        return;
    }
    screenIsReady = true;
    secondVisibilityGroup = snapContainer.snap.select('#visible-group-2').attr({opacity: 0});
    thirdVisibilityGroup = snapContainer.snap.select('#visible-group-3').attr({opacity: 0});
    fourthVisibilityGroup = snapContainer.snap.select('#visible-group-4').attr({opacity: 0});
}

const ShowBlocks = () => {
    secondVisibilityGroup.animate({
        opacity: 1
    }, 800, mina.easeinout, () => thirdVisibilityGroup.animate({
        opacity: 1
    }, 800, mina.easeinout, () => fourthVisibilityGroup.animate({
        opacity: 1
    }, 800, mina.easeinout)));
};