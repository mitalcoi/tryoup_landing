import Snap from 'snapsvg';
import _ from 'underscore';
import {isMobile} from './tools';
import ScrollListener from './scrollListener';
import SnapContainer from './snapContainer';

let isActivePage = false;
let isReadyPage = false;

let paper = null;
let snapContainer = null;
let currentScroll = 0;

let circleElement = null;
let circlePath = null;
let triangleElement = null;
let trianglePath = null;
let trashElement = null;
let trashPath = null;

let chart = null;
let chartBBox = null;
let clientSmile = null;
let clientSmileBBox = null;
let stars = null;

let sectionMovedDown = false;
let isFinalAnimationReversed = true;

let pageIndex = 0;
const scrollOffset = .05;
const finalScroll = .96;

export default (index = 0) => {
    pageIndex = index;
    const containerID = '#customersContainer';
    const snapContainerID = '#customersSnapContainer';
    const svgName = 'customers-screen-01-01-01.svg';
    const mobileSvgName = 'customers-screen-mob.svg';
    const scrollListener = window.scrollListener;

    snapContainer = new SnapContainer(containerID, snapContainerID, (isMobile.any() ? mobileSvgName : svgName), () => {
        SVGReadyHandler();

        let circleHidden = false;
        let triangleHidden = false;

        !isMobile.any() && scrollListener.onLocalScroll((event, scrollData) => {

            currentScroll = scrollData.relative - scrollOffset;
            UpdateElementsPosition();

            // CIRCLE HIDE
            if (scrollData.relative > .3746 && scrollData.relative < .4146) {
                !circleHidden && circleElement.removeClass('show').addClass('hide');
                circleHidden = true;
            } else {
                circleHidden && circleElement.removeClass('hide').addClass('show');
                circleHidden = false;
            }

            // TRIANGLE HIDE
            if (scrollData.relative > .3635 && scrollData.relative < .4146) {
                !triangleHidden && triangleElement.removeClass('show').addClass('hide');
                triangleHidden = true;
            } else {
                triangleHidden && triangleElement.removeClass('hide').addClass('show');
                triangleHidden = false;
            }

            if (scrollData.relative >= finalScroll && isFinalAnimationReversed) {
                FinalAnimation();
            } else if (scrollData.relative < finalScroll && !isFinalAnimationReversed) {
                FinalAnimationReversed();
            }
        });
    });

    if (isMobile.any()) {
        $('.customers-screen').find('.description').removeClass('hide').addClass('show');
        snapContainer.snap.removeClass('hide').addClass('show'); 
    } else {
        scrollListener.onMove((event, data) => {
            if (data.nextIndex == pageIndex && data.direction == 'down') {
                $('.customers-screen').find('.description').removeClass('hide').addClass('show');
                snapContainer.snap.removeClass('hide').addClass('show'); 
            }
    
            if (data.nextIndex == pageIndex - 1) {
                $('.customers-screen').find('.description').removeClass('show').addClass('hide');
                snapContainer.snap.removeClass('show').addClass('hide'); 
                isActivePage = false;
            }
    
            if (data.nextIndex == pageIndex + 1) {
                isActivePage = false;
            }
        });
    
        scrollListener.onStop((event, data) => {
            if (data.index == pageIndex) {
                isActivePage = true;
                if (!isReadyPage) return;
                AnimateGeoTag();
                AnimateEyes();
                AnimateDispatchers();
            }
        });
    }

    
};

const SVGReadyHandler = () => {
    paper = snapContainer.snap.paper;
    isReadyPage = true;
    circleElement = paper.select('#circle');
    circlePath = paper.select('#circle-path');
    triangleElement = paper.select('#triangle');
    trianglePath = paper.select('#triangle-path');
    trashElement = paper.select('#trash');
    trashPath = paper.select('#trash-path');

    if (isMobile.any()) {
        return;
    }

    chart = paper.select('#chart');
    chartBBox = chart.getBBox();
    chart.attr({
        transform: 's 1 0 '+chartBBox.cx+' '+(chartBBox.cy + chartBBox.height / 2)
    });

    stars = paper.selectAll('#happy-client g[id^="star-"]').attr({
        opacity: 0
    });

    clientSmile = paper.select('#happy-client #smile');
    clientSmileBBox = clientSmile.getBBox();
    clientSmile.attr({
        opacity: 0,
        transform: 's 1 .2 '+clientSmileBBox.cx+' '+(clientSmileBBox.cy + clientSmileBBox.height / 2)
    });
};

const UpdateElementsPosition = () => {
    const circleDistance = (circlePath.getTotalLength() * currentScroll) + (circlePath.getTotalLength() * (scrollOffset + scrollOffset)) * currentScroll;
    const circleMove = circlePath.getPointAtLength(circleDistance);
    const circleStart = circlePath.getPointAtLength(0);
    circleElement.transform('t '+(circleMove.x - circleStart.x)+', '+(circleMove.y - circleStart.y));

    const trashDistance = trashPath.getTotalLength() * Math.min(1, (circleDistance / trashPath.getTotalLength()));
    const trashMove = trashPath.getPointAtLength(trashDistance);
    const trashStart = trashPath.getPointAtLength(0);
    trashElement.transform('t '+(trashMove.x - trashStart.x)+', '+(trashMove.y - trashStart.y));

    const triangleDistance = trianglePath.getTotalLength() * Math.min(1, (circleDistance / trianglePath.getTotalLength()));
    const triangleMove = trianglePath.getPointAtLength(triangleDistance);
    const triangleStart = trianglePath.getPointAtLength(0);
    triangleElement.transform('t '+(triangleMove.x - triangleStart.x)+', '+(triangleMove.y - triangleStart.y));

    if (Math.min(1, (circleDistance / trianglePath.getTotalLength())) == 1) {
        triangleElement.attr({
            opacity: 0
        });
    } else {
        triangleElement.attr({
            opacity: 1
        });
    }
};

const AnimateDashedPaths = () => {
    const paths = paper.selectAll('#dashed-paths path');
    paths.forEach(path => {
        const pathAnimation = () => {
            path.attr({
                'stroke-dashoffset': 0
            });
            Snap.animate(0, path.getTotalLength(), step => {
                path.attr({
                    'stroke-dashoffset': -step
                });
            }, 20000, mina.linear, (isActivePage ? () => pathAnimation() : () => {}));
        }
        pathAnimation();
    });
};

const AnimateGeoTag = () => {
    const geoTag = paper.select('#geo-tag');
    const shape = geoTag.select('path');
    const wave = geoTag.select('ellipse');

    const waveBBox = wave.getBBox();

    const shapeMaxHeight = 20;

    const geoTagAnimation = () => {
        
        wave.transform('s 0 0'+waveBBox.cx+' '+waveBBox.cy);

        const countShapeStep = (step) => {
            if (step < .5) {
                return (shapeMaxHeight * step);
            } else {
                return (shapeMaxHeight * (1 - step));
            }
        }

        Snap.animate(0, 1, step => {
            shape.attr({
                transform: 't '+0+' '+(0 - countShapeStep(step))
            });
            wave.attr({
                opacity: (1 - step),
                transform: 's '+step+' '+step+' '+waveBBox.cx+' '+waveBBox.cy
            });
        }, 600, mina.easein, (isActivePage ? () => geoTagAnimation() : () => {}));
    };

    geoTagAnimation();
};

const AnimateEyes = () => {
    const circleCustomer = paper.select('#circle-customer');
    const triangleCustomer = paper.select('#triangle-customer');
    const trashCustomer = paper.select('#trash-customer');
    const happyClient = paper.select('#happy-client');
    const serviceMan = paper.select('#serviceman');
    const circleDispatcher = paper.select('#dispatchers #circle-dispatcher');
    const triangleDispatcher = paper.select('#dispatchers #triangle-dispatcher');

    const randomRange = {
        from: 500,
        to: 3000
    };

    let eyesArray = [];
    eyesArray.push(circleCustomer.selectAll('path[id^="left-eye"], path[id^="right-eye"]'));
    eyesArray.push(triangleCustomer.selectAll('path[id^="left-eye"], path[id^="right-eye"]'));
    eyesArray.push(trashCustomer.selectAll('path[id^="left-eye"], path[id^="right-eye"]'));
    eyesArray.push(serviceMan.selectAll('path[id^="left-eye"], path[id^="right-eye"]'));
    eyesArray.push(happyClient.selectAll('g[id^="left-eye"], g[id^="right-eye"]'));
    eyesArray.push(circleDispatcher.selectAll('g[id^="left-eye"], g[id^="right-eye"]'));
    eyesArray.push(triangleDispatcher.selectAll('g[id^="left-eye"], g[id^="right-eye"]'));

    eyesArray.forEach(set => {
        const firstEye = set[0];
        const secondEye = set[1];
        const firstEyeBBox = firstEye.getBBox();
        const secondEyeBBox = secondEye.getBBox();

        const eyeAnimation = () => {
            Snap.animate(-1, 1, step =>{
                firstEye.transform('s 1 '+1 * (step < 0 ? (1 + step) : step)+' '+firstEyeBBox.cx+' '+firstEyeBBox.cy);
                secondEye.transform('s 1 '+1 * (step < 0 ? (1 + step) : step)+' '+secondEyeBBox.cx+' '+secondEyeBBox.cy);
            }, 300, mina.easein, (!isActivePage ? () => {} : () => {
                setTimeout(() => eyeAnimation(), _.random(randomRange.from, randomRange.to));
            }));
        }

        eyeAnimation();
    });
};

let isCircleAnimating = false;
let isTriangleAnimating = false;
const AnimateDispatchers = () => {
    if (isCircleAnimating || isTriangleAnimating) return;
    const dispatchers = paper.select('#dispatchers');
    const circleDispatcher = dispatchers.select('#circle-dispatcher');
    const triangleDispatcher = dispatchers.select('#triangle-dispatcher');

    const animateCircleDispatcher = (amplitude) => {
        if (isCircleAnimating) return;
        isCircleAnimating = true;
        const leftHand = circleDispatcher.select('g[id^="left-hand"]');
        const rightHand = circleDispatcher.select('g[id^="right-hand"]');
        
        const animateHands = () => {
            Snap.animate(-1, 1, step => {
                leftHand.transform('t '+amplitude * (step < 0 ? (1 + step) : step)+' 0');
                rightHand.transform('t '+(-amplitude * (step < 0 ? (1 + step) : step))+' 0');
            }, 500, mina.easeinout, (isActivePage ? () => {
                isCircleAnimating = false;
                animateHands();
            } : () => {isCircleAnimating = false;}));
        }
        animateHands();
    };
    
    const animateTriangleDispatcher = (amplitude) => {
        if (isTriangleAnimating) return;
        isTriangleAnimating = true;
        const head = triangleDispatcher.select('#head');
        const headBBox = head.getBBox();
        const animationTime = 1200;
        const waitTime = 1000;
        const pivot = {
            x: headBBox.cx,
            y: headBBox.cy + headBBox.height / 2
        }

        const animateHead = () => {
            Snap.animate(0, 1, step => {
                head.transform('r '+amplitude * step+' '+pivot.x+' '+pivot.y)
            }, animationTime / 2, mina.linear, () => setTimeout(() => {
                Snap.animate(1, -1, step => {
                    head.transform('r '+amplitude * step+' '+pivot.x+' '+pivot.y)
                }, animationTime, mina.linear, () => setTimeout(() =>{
                    Snap.animate(-1, 0, step => {
                        head.transform('r '+amplitude * step+' '+pivot.x+' '+pivot.y)
                    }, animationTime / 2, mina.linear, (isActivePage ? () => {
                        isTriangleAnimating = false;
                        animateHead();
                    } : () => {isTriangleAnimating = false;}));
                }, waitTime));
            }, waitTime));
        };
        animateHead();
    };

    animateCircleDispatcher(1);
    animateTriangleDispatcher(15);
};

const AnimateStars = () => {
    const starsCount = stars.length;
    const starsPerTic = 3;
    let animateInterval = null;

    const AnimateStar = (star) => {
        const starBBox = star.getBBox();
        const maxScale = 1.3;
        star.animate({
            transform: 's '+maxScale+' '+maxScale+' '+starBBox.cx+' '+starBBox.cy,
            opacity: .8
        }, 300, mina.easeinout, () => {
            star.animate({
                transform: 's .2 .2 '+starBBox.cx+' '+starBBox.cy,
                opacity: 0
            }, 300, mina.easeinout, () => {
                star.remove();
            });
        });
    };

    animateInterval = setInterval(() => {
        let counter = 0;
        while (counter++ < starsPerTic) {
            const randIndex = _.random(0, starsCount - 1);
            AnimateStar(stars[randIndex].clone());
        }
        
        if (currentScroll < finalScroll - scrollOffset || !isActivePage) {
            clearInterval(animateInterval);
        }
    }, 300);
};

const FinalAnimation = () => {
    isFinalAnimationReversed = false;
    AnimateStars();
    Snap.animate(0, 1.2, step => {
        clientSmile.attr({
            transform: 's 1 '+step+' '+clientSmileBBox.cx+' '+(clientSmileBBox.cy + clientSmileBBox.height / 2),
            opacity: step
        });
        chart.transform('s 1 '+step+' '+chartBBox.cx+' '+(chartBBox.cy + chartBBox.height / 2));
    }, 300, mina.easeinout);
};

const FinalAnimationReversed = () => {
    isFinalAnimationReversed = true
    Snap.animate(1.2, 0, step => {
        clientSmile.attr({
            transform: 's 1 '+step+' '+clientSmileBBox.cx+' '+(clientSmileBBox.cy + clientSmileBBox.height / 2),
            opacity: step
        });
        chart.transform('s 1 '+step+' '+chartBBox.cx+' '+(chartBBox.cy + chartBBox.height / 2));
    }, 300, mina.easeinout);
};