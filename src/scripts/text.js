import ScrollListener from './scrollListener';
import {isMobile} from './tools';

let pageIndex = 0;

export default (index = 0) => {
    pageIndex = index;
    const scrollListener = window.scrollListener;

    if (!isMobile.any()) {
        scrollListener.onMove((event, data) => {
            if (data.nextIndex == pageIndex) {
                $('.text-screen').find('.description').removeClass('hide').addClass('show');
            }
    
            if (data.nextIndex == pageIndex - 1) {
                $('.text-screen').find('.description').removeClass('show').addClass('hide');
            }
        });
    } else  {
        $('.text-screen').find('.description').removeClass('hide').addClass('show');
    }
    
};