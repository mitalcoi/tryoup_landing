import Snap from 'snapsvg';
import SnapContainer from './snapContainer';
import _ from 'underscore';

let snapContainer = null;
const containerID = '#figuresContainer';
const snapContainerID = '#figuresSnapContainer';
const svgName = 'contacts-figures.svg';

let figures = null;

export default () => {

    snapContainer = new SnapContainer(containerID, snapContainerID, svgName, SVGReadyHandler);

    $('.contacts-screen').find('.description').addClass('show');
    $('.contacts-screen').find('.input-field').change((event) => {
        const target = event.target;
        const value = target.value;
        if (value.length) {
            $(target).parent('.input-field').addClass('not-empty');
        } else {
            $(target).parent('.input-field').removeClass('not-empty');
        }
    });
};

const SVGReadyHandler = () => {
    snapContainer.snap.removeClass('hide').addClass('show');
    figures = snapContainer.snap.selectAll('ellipse, rect, polygon');
    const itemMovementRange = {
        x: {
            min: -15,
            max: 15
        },
        y: {
            min: -15,
            max: 15
        }
    };
    let currentDestination = {x: 0, y: 0};
    
    figures.forEach(item => {
        const itemBBox = item.getBBox();
        const animate = () => {
            currentDestination = {
                x: _.random(itemMovementRange.x.min, itemMovementRange.x.max),
                y: _.random(itemMovementRange.y.min, itemMovementRange.y.max)
            }

            item.animate({
                transform: 'T '+currentDestination.x+' '+currentDestination.y+' r '+_.random(0, 30)+' '+itemBBox.cx+' '+itemBBox.cy
            }, _.random(2000, 4000), mina.linear, animate)
        };
        animate();
    });
}