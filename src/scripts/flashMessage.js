import _ from 'underscore';

class FlashMessage {
    
    constructor(intent, message) {
        this.templateID = 'flash-message-' + _.now();
        this.timeoutID = -1;
        this.timeToHide = 3000;
        this.messageTemplate = '<div id="' + this.templateID + '" class="flash-message ' + intent + '">'
            +'<span class="flash-message-icon"></span>'
            // +'<p class="flash-message-title">' + intent + '</p>'
            +'<p class="flash-message-text">' + message + '</p>'
        +'</div>';

        this.show();
    }

    show() {
        $('body').append(this.messageTemplate);
        window.setTimeout(() => this.hide(this.templateID), this.timeToHide);
    }

    hide(templateID) {
        $('#' + templateID).remove();
    }
}

export default FlashMessage;