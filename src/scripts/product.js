import Snap from 'snapsvg';
import ScrollListener from './scrollListener';
// import _ from 'underscore';
import SnapContainer from './snapContainer';
import {isMobile} from './tools';

let snapContainer = null;
let pageIndex = 0;
let screenIsReady = false;

export default (index = 0) => {
    pageIndex = index;
    const containerID = '#productContainer';
    const snapContainerID = '#productSnapContainer';
    const svgName = 'product-scheme.svg';
    const scrollListener = window.scrollListener;

    if (isMobile.any()) {
        $('.product-screen').removeClass('hide').addClass('show');
    } else {
        scrollListener.onMove((event, data) => {
            if (data.nextIndex == pageIndex) {
                $('.product-screen').removeClass('hide').addClass('show');
            }
    
            if (data.nextIndex == pageIndex - 1) {
                $('.product-screen').removeClass('show').addClass('hide');
            }
        });
    }
};

let secondVisibilityGroup = null;
let thirdVisibilityGroup = null;
let fourthVisibilityGroup = null;
let picture = null;

let arrow = null;
let firstArrowPath = null;
let secondArrowPath = null;
let thirdArrowPath = null;
let firstArrow = null;
let secondArrow = null;
let thirdArrow = null;

const SVGReadyHandler = () => {
    screenIsReady = true;
    secondVisibilityGroup = snapContainer.snap.select('#visible-group-2').attr({opacity: 0});
    thirdVisibilityGroup = snapContainer.snap.select('#visible-group-3').attr({opacity: 0});
    fourthVisibilityGroup = snapContainer.snap.select('#visible-group-4').attr({opacity: 0});
}

const ShowBlocks = () => {
    secondVisibilityGroup.animate({
        opacity: 1
    }, 800, mina.easeinout, () => thirdVisibilityGroup.animate({
        opacity: 1
    }, 800, mina.easeinout, () => fourthVisibilityGroup.animate({
        opacity: 1
    }, 800, mina.easeinout)));
};

const buildInitialPath = (path) => {
    return snapContainer.snap.path({
        path: path.getSubpath(0, 0),
        strokeWidth: 3,
        stroke: '#C03C54'
    });
};

const StartAnimate = () => {
    const firstSubPath = buildInitialPath(firstArrowPath);

    Snap.animate(0, firstArrowPath.getTotalLength(), step => {
        const startPathPoint = firstArrowPath.getPointAtLength(0);
        firstSubPath.attr({
            path: firstArrowPath.getSubpath(0, step)
        });
        const arrowStart = firstArrowPath.getPointAtLength(0);
        const arrowMove = firstArrowPath.getPointAtLength(step);
        firstArrow.attr({strokeWidth: 3});
        firstArrow.transform('t '+(arrowMove.x - arrowStart.x)+', '+(arrowMove.y - arrowStart.y));
    }, 800, mina.easeinout, () => {
        secondVisibilityGroup.animate({
            opacity: 1
        }, 800, () => {
            AnimateSecond();
        });
    });
};

const AnimateSecond = () => {
    const secondSubPath = buildInitialPath(secondArrowPath);

    Snap.animate(0, secondArrowPath.getTotalLength(), step => {
        const startPathPoint = secondArrowPath.getPointAtLength(0);
        secondSubPath.attr({
            path: Snap.path.getSubpath(secondArrowPath, 0, step)
        });
        const arrowStart = secondArrowPath.getPointAtLength(0);
        const arrowMove = secondArrowPath.getPointAtLength(step);
        secondArrow.attr({strokeWidth: 3});
        secondArrow.transform('t '+(arrowMove.x - arrowStart.x)+', '+(arrowMove.y - arrowStart.x)+' r '+arrowMove.alpha);
    }, 2000, mina.easeinout, () => {
        thirdVisibilityGroup.animate({
            opacity: 1
        }, 800, () => {
            // AnimateSecond();
        });
    });
};