import {isMobile} from './tools';

export default () => {
    const scrollOffset = 0;
    if (isMobile.any()) {
        $('header').addClass('is-mobile');
        return;
    }
    $(window).scroll(() => {
        if ($(window).scrollTop() > scrollOffset) {
            $('header').addClass('compact');
        } else {
            $('header').removeClass('compact');
        }
    });
};