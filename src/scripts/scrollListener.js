require('velocity-animate');
import {isMobile} from './tools';
import _ from 'underscore';

class ScrollListener {

    constructor(container) {
        this.currentIndex = 0;
        this.$mainContainer = $(container);
        this.$containers = $('.section');
        this.eventsBlocked = false;
        this.scrollSwitcher = (() => {
            // left: 37, up: 38, right: 39, down: 40,
            // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
            const keys = {37: 1, 38: 1, 39: 1, 40: 1};

            const preventDefault = (e) => {
                e = e || window.event;
                e.stopPropagation && e.stopPropagation();
                e.stopImmediatePropagation && e.stopImmediatePropagation();
                e.preventDefault && e.preventDefault();
                e.returnValue = false;  
            }
            const preventDefaultForScrollKeys = (e) => {
                if (keys[e.keyCode]) {
                    preventDefault(e);
                    return false;
                }
            }

            return {
                disableScroll: () => {
                    $('body').addClass('blockScroll');
                    if (window.addEventListener) // older FF
                        window.addEventListener('DOMMouseScroll', preventDefault, false);
                    $(window).on('scroll', preventDefault);
                    window.onwheel = preventDefault; // modern standard
                    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
                    window.ontouchmove  = preventDefault; // mobile
                    document.onkeydown  = preventDefaultForScrollKeys;
                    this.isScrollDisabled = true;
                },
                enableScroll: () => {
                    // console.log('enable');
                    $('body').removeClass('blockScroll');
                    if (window.removeEventListener)
                        window.removeEventListener('DOMMouseScroll', preventDefault, false);
                    $(window).off('scroll', preventDefault);
                    window.onmousewheel = document.onmousewheel = null; 
                    window.onwheel = null; 
                    window.ontouchmove = null;  
                    document.onkeydown = null;  
                    this.isScrollDisabled = false;
                }
            }
        })();

        $(window).scroll(event => _.throttle(event => {
            
            const $container = $(this.$containers[this.currentIndex]);
            const globalScroll = $(window).scrollTop();
            const offsetTop = $container.offset().top;
            const containerHeight = $container.outerHeight();
            const viewportHeight = $(window).height();

            //***TRIGGER EVENTS***//

            // MOVE TOP
            if (globalScroll < (offsetTop - viewportHeight * .15)) {
                // console.log('top');
                if (this.currentIndex <= 0) {
                    return;
                }
                this.$mainContainer.trigger('move', {
                    index: this.currentIndex,
                    nextIndex: this.currentIndex - 1,
                    direction: 'up'
                });
                !this.eventsBlocked && this.scrollToIndex(this.currentIndex - 1);
            }

            // MOVE BOTTOM
            if (globalScroll > (offsetTop + containerHeight - viewportHeight * .85)) {
                // console.log('bottom');
                if (this.currentIndex >= this.$containers.length - 1) {
                    return;
                }
                this.$mainContainer.trigger('move', {
                    index: this.currentIndex,
                    nextIndex: this.currentIndex + 1,
                    direction: 'down'
                });
                !this.eventsBlocked && this.scrollToIndex(this.currentIndex + 1);
            }

            // LOCAL SCROLL AMOUNT
            if (containerHeight > viewportHeight && !this.eventsBlocked) {
                const scrollAmount = Math.min(Math.max(0, globalScroll - offsetTop), containerHeight - viewportHeight);
                this.$mainContainer.trigger('localScroll', {
                    index: this.currentIndex,
                    absolute: scrollAmount,
                    relative: scrollAmount / (containerHeight - viewportHeight)
                });
            }
        }, 10)(event));
    }

    onMove(listener) {
        this.$mainContainer.on('move', listener);
    }

    onStop(listener) {
        this.$mainContainer.on('stop', listener);
    }

    onLocalScroll(listener) {
        this.$mainContainer.on('localScroll', listener);
    }

    scrollToIndex(index) {
        // console.log('scroll');
        this.eventsBlocked = true;
        this.scrollSwitcher.disableScroll();
        let offset = 0;
        const $container = $(this.$containers[index]);

        if (index < this.currentIndex) {
            const containerHeight = $container.outerHeight();
            const viewportHeight = $(window).height();
            offset = containerHeight - viewportHeight;
        }

        $container.velocity('scroll', {
            duration: 800,
            offset: offset,
            easing: 'linear',
            complete: () => {
                this.eventsBlocked = false;
                this.scrollSwitcher.enableScroll();
                this.currentIndex = index;
                this.$mainContainer.trigger('stop', {
                    index: index
                });
            }
        });
    }

    scrollNext() {
        this.scrollToIndex(this.currentIndex + 1);
    }

    scrollToLast() {
        this.scrollToIndex(this.$containers.length - 1);
    }
}

export default ScrollListener;