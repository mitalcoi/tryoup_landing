import Snap from 'snapsvg';

class SnapContainer {

    constructor(containerID, snapContainerID, svgName, onReadyState) {

        this.$container = $(containerID);
        this.snap = Snap(snapContainerID);
        if (!this.snap) return;

        Snap.load('./media/svg/'+svgName, fragment => {

            const screen = fragment.select('svg > g');

            let fragmentViewBox = null;

            if (fragment.node.viewBox) {
                fragmentViewBox = fragment.node.viewBox.baseVal;
            } else {
                const childNodes = fragment.node.childNodes;
                const svgElementKey = Object.keys(fragment.node.childNodes).find(key => {
                    if (childNodes[key].viewBox) {
                        return key > 1;
                    }

                    return false;
                });

                fragmentViewBox = svgElementKey ? childNodes[svgElementKey].viewBox.baseVal : null;
            }

            if (!fragmentViewBox) {
                console.warn('Fragment\'s viewBox not found !!!');
                return;
            }

            const aspectRatio = fragmentViewBox.width / fragmentViewBox.height;

            const snapWidth = this.$container.width();
            const snapHeight = snapWidth / aspectRatio;

            this.snap.attr({
                width: '100%',
                height: '100%',
                viewBox: '0 0 ' + snapWidth + ' ' + snapHeight
            });

            this.snap.add(screen);

            const innerBBox = screen.getBBox();

            const scaleFactor = {
                x: snapWidth / innerBBox.width,
                y: snapHeight / innerBBox.height
            };

            screen.transform(
                't'+(snapWidth - innerBBox.width) / 2+' '
                +(snapHeight - innerBBox.height) / 2+' '
                +'s'+(scaleFactor.x > scaleFactor.y ? scaleFactor.y : scaleFactor.x)
            );

            onReadyState();
        });
    }
}

export default SnapContainer;