import Snap from 'snapsvg';
import SnapContainer from './snapContainer';
import _ from 'underscore';
import {isMobile} from './tools';

let snapContainer = null;
const containerID = '#integrationContainer';
const snapContainerID = '#integrationSnapContainer';
const svgName = 'integration-screen.svg';

let pageIndex = 0;

export default (index = 0) => {
    pageIndex = index;
    snapContainer = new SnapContainer(containerID, snapContainerID, svgName, SVGReadyHandler);
    if (isMobile.any()) {
        $('.integration-screen').find('.description').removeClass('hide').addClass('show');
        snapContainer.snap.removeClass('hide').addClass('show');
    } else {
        scrollListener.onMove((event, data) => {
            if (data.nextIndex == pageIndex) {
                $('.integration-screen').find('.description').removeClass('hide').addClass('show');
                snapContainer.snap.removeClass('hide').addClass('show');
            }
            if (data.nextIndex == pageIndex - 1) {
                $('.integration-screen').find('.description').removeClass('show').addClass('hide');
                snapContainer.snap.removeClass('show').addClass('hide');
            }
        });
    }
    
};

const SVGReadyHandler = () => {
    const paper = snapContainer.snap.select('#integration-screen');
    const paperBBox = paper.getBBox();

    const mainLogo = paper.select('g#logo-0');
    const mainLogoBBox = mainLogo.getBBox();
    const mainCircle = mainLogo.select('circle');
    const mainCircleBBox = mainCircle.getBBox();
    const mainNode = mainCircle.clone();

    mainNode.attr({ fill: 'white' });
    mainNode.transform(Snap.format('t {x} {y} s 1.05 1.05 {cx} {cy}', {
        x: mainLogoBBox.x,
        y: mainLogoBBox.y,
        cx: mainCircleBBox.cx,
        cy: mainCircleBBox.cy
    }));

    const logos = paper.selectAll('g#logos > g');
    const nodes = paper.g(mainNode);
    const lines = paper.g();

    paper.select('g#logos').insertAfter(nodes);
    mainLogo.insertBefore(paper.select('g#logos'));
    lines.insertBefore(nodes);

    logos.forEach((logo, index) => {
        const circle = logo.select('circle');
        const logoBBox = logo.getBBox();
        const circleBBox = circle.getBBox();

        const node = circle.clone();
        node.attr({fill: 'white'});
        node.transform(Snap.format('t {x} {y} s 1.05 1.05 {cx} {cy}', {
            x: logoBBox.x + 5,
            y: logoBBox.y + 5,
            cx: circleBBox.cx,
            cy: circleBBox.cy
        }));
        nodes.add(node);
        const nodeBBox = node.getBBox();
        logo.hover(
            () => handleLogoHoverOn(logo, logoBBox, node, nodeBBox), 
            () => handleLogoHoverOut(logo, logoBBox, node, nodeBBox)
        );

        const line = paper.path(Snap.format('M {fromX} {fromY} L {toX} {toY}', {
            fromX: mainLogoBBox.x + mainLogoBBox.w / 2,
            fromY: mainLogoBBox.y  + mainLogoBBox.h / 2,
            toX: logoBBox.x + logoBBox.w / 2,
            toY: logoBBox.y + logoBBox.h / 2
        })).attr({
            stroke: 'white',
            strokeWidth: 5
        });

        lines.add(line);
    });
    
    const maskGroup = paper.g(nodes, lines);
    const gradientImage = paper.image('./media/gradient-mask.png', -15, -15, paperBBox.w + 40, paperBBox.h + 40);
    gradientImage.insertBefore(mainLogo);
    gradientImage.attr({
        mask: maskGroup
    });
}

const handleLogoHoverOn = (logo, logoBBox, node, nodeBBox) => {
    const animationSpeed = 100;
    const scaleFactor = .2;

    logo.animate({
        transform: Snap.format('T {x} {y} s {scaleX} {scaleY} {cx} {cy}', {
            x: logoBBox.x,
            y: logoBBox.y,
            scaleX: 1 + scaleFactor, 
            scaleY: 1 + scaleFactor,
            cx: logoBBox.w / 2,
            cy: logoBBox.h / 2
        })
    }, animationSpeed);
    node.animate({
        transform: Snap.format('T {x} {y} s  {scaleX} {scaleY} {cx} {cy}', {
            x: nodeBBox.x + 5,
            y: nodeBBox.y + 5,
            scaleX: 1.05 + scaleFactor, 
            scaleY: 1.05 + scaleFactor,
            cx: nodeBBox.w / 2,
            cy: nodeBBox.h / 2
        })
    }, animationSpeed);
}

const handleLogoHoverOut = (logo, logoBBox, node, nodeBBox) => {
    const animationSpeed = 100;
    
    logo.animate({
        transform: Snap.format('T {x} {y} s 1 1 {cx} {cy}', {
            x: logoBBox.x,
            y: logoBBox.y,
            cx: logoBBox.w / 2,
            cy: logoBBox.h / 2
        })
    }, animationSpeed);
    node.animate({
        transform: Snap.format('T {x} {y} s 1.05 1.05 {cx} {cy}', {
            x: nodeBBox.x + 4,
            y: nodeBBox.y + 4,
            cx: nodeBBox.w / 2,
            cy: nodeBBox.h / 2
        })
    }, animationSpeed);
}