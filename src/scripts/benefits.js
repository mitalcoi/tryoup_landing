import Snap from 'snapsvg';
import ScrollListener from './scrollListener';
import _ from 'underscore';
import SnapContainer from './snapContainer';
import {isMobile} from './tools';

let pageIndex = -1;
let chartContainer = null;
let sunsetContainer = null;
let currentMousePos = null;

const cloudsOffsetFactor = .05;

let StartChartAnimation = null;
let StopChartAnimation = null;

export default (index = 0) => {
    pageIndex = index;
    const chartId = '#chartContainer';
    const chartSnapID = '#chartSnapContainer';
    const chartName = 'chart-screen-01.svg';

    const sunsetId = '#sunsetContainer';
    const sunsetSnapID = '#sunsetSnapContainer';
    const sunsetName = 'sunset-screen.svg';

    chartContainer = new SnapContainer(chartId, chartSnapID, chartName, ChartReadyHandler);
    sunsetContainer = new SnapContainer(sunsetId, sunsetSnapID, sunsetName, SunsetReadyHandler);
    InitLocator();

    if (isMobile.any()) {
        $('#locatorContainer .locator').removeClass('hide').addClass('show');
        chartContainer.snap.removeClass('hide').addClass('show');
        sunsetContainer.snap.removeClass('hide').addClass('show');
    } else {
        scrollListener.onMove((event, data) => {
            if (data.nextIndex == pageIndex) {
                $('#locatorContainer .locator').removeClass('hide').addClass('show');
                chartContainer.snap.removeClass('hide').addClass('show');
                sunsetContainer.snap.removeClass('hide').addClass('show');
            }
    
            if (data.nextIndex == pageIndex - 1) {
                $('#locatorContainer .locator').removeClass('show').addClass('hide');
                chartContainer.snap.removeClass('show').addClass('hide');
                sunsetContainer.snap.removeClass('show').addClass('hide');
            }
        });
    
        scrollListener.onStop((event, data) => {
            if (data.index == pageIndex) {
                StartChartAnimation();
            }
        });
    }
};

const InitLocator = () => {

    const $container = $('#locatorContainer');
    const $locator = $container.find('.locator');
    
    //DEFINE LOCATOR SIZE
    let size = Math.min($container.width(), $container.height());
    $locator.width(size);
    $locator.height(size);

    $(window).resize(_.throttle(() => {
        size = Math.min($container.width(), $container.height());
        $locator.width(size);
        $locator.height(size);
    }, 100));
};

const ChartReadyHandler = () => {
    const arrow = chartContainer.snap.select('#chart-arrow');
    const gradient = chartContainer.snap.select('#chart-mask');
    const path = arrow.select('path').attr({strokeWidth:0});
    const polygon = arrow.select('polygon');
    let chartTimeout = null;
    let isAnimating = false;
    const subpath = chartContainer.snap.path({
        path: Snap.path.getSubpath(path, 0, path.getTotalLength()),
        fill: 'none',
        strokeWidth: 8,
        stroke: '#fff'
    });

    const mask = chartContainer.snap.g(polygon, subpath);
    gradient.attr({
        mask: mask
    });

    if (isMobile.any()) {
        subpath.attr({
            path: Snap.path.getSubpath(path, 0, path.getTotalLength()),
        });
        return;
    }

    let animationCallBack = null;

    StartChartAnimation = () => {
        animationCallBack = () => Snap.animate(0, path.getTotalLength(), step => {

            subpath.attr({
                path: Snap.path.getSubpath(path, 0, step),
            });

            const polygonEnd = path.getPointAtLength(path.getTotalLength());
            const polygonMove = path.getPointAtLength(step);
            polygon.transform('t '+(polygonMove.x - polygonEnd.x)+', '+(polygonMove.y - polygonEnd.y - 2)+' r ' + (polygonMove.alpha - 120));
            }, 2000,  mina.easeInOut, () => {}
        );
        animationCallBack();
    }
};

const SunsetReadyHandler = () => {
    const clouds = sunsetContainer.snap.selectAll('path[id^="cloud-"]');
    const cloudsGradient = sunsetContainer.snap.gradient('l(0, 0, 0, 1)rgba(208,103,76,1)-rgba(208,103,76,0)');
    clouds.attr({
        fill: cloudsGradient
    });

    if (isMobile.any()) {
        return;
    }

    const svg = document.querySelector('#sunsetSnapContainer');
    const pt = svg.createSVGPoint();
    const cursorPoint = event =>{
        pt.x = event.clientX; pt.y = event.clientY;
        return pt.matrixTransform(svg.getScreenCTM().inverse());
    }

    sunsetContainer.snap.mousemove(_.throttle(event => {
        const localMousePos = cursorPoint(event);
        const constainerSize = {
            x: $('#sunsetSnapContainer').width(),
            y: $('#sunsetSnapContainer').height()
        };
        const center = { x: constainerSize.x / 2, y: constainerSize.y / 2};
        const clampedMousePos = {
            x: Math.min(Math.max(0, localMousePos.x), constainerSize.x),
            y: Math.min(Math.max(0, localMousePos.y), constainerSize.y)
        };
        const mouseDirection = {
            x: clampedMousePos.x - center.x,
            y: clampedMousePos.y - center.y
        };
        const cloudsDirection = {
            x: mouseDirection.x * (-1),
            y: mouseDirection.y * (-1)
        };

        clouds.forEach((item, index) => {
            item.transform(
                't '+(cloudsDirection.x * (index * cloudsOffsetFactor + cloudsOffsetFactor) + 60)+
                ', '+(cloudsDirection.y * (index * cloudsOffsetFactor + cloudsOffsetFactor) + 10)
            );
        });
    }, 50));
};