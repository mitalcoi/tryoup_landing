const pageIndex = 1;
let $faqItems = null;
let currentActive = -1;

export default () => {
    
    $('.faq-screen').find('.description').addClass('show');

    $faqItems = $('.faq-item');
    $faqItems.click(ItemClickHandler);
};

const ItemClickHandler = function(event) {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active').addClass('inactive');
    } else if (currentActive < 0) {
        $(this).removeClass('inactive').addClass('active');
    }
};