import Snap from 'snapsvg';
import _ from 'underscore';
import ScrollListener from './scrollListener';
import SnapContainer from './snapContainer';

const pageIndex = -1;

let snapContainer = null;
let maxRotationAmount = 360;
let currentRotationAmount = 0;

let startAnimation = null;
let stopAnimation = null;

let gearsList = [];
let gearsBBoxes = [];
let isFinished = false;
let shapes = null;
let shapesBBoxes = [];
let shapesDestinations = [];

export default () => {
    const containerID = '#gearsContainer';
    const snapContainerID = '#gearsSnapContainer';
    const svgName = 'gears-screen.svg';
    const $screen = $('.gears-screen');

    snapContainer = new SnapContainer(containerID, snapContainerID, svgName, SVGReadyHandler);
    const scrollListener = window.scrollListener;

    scrollListener.onMove((event, data) => {
        if (data.nextIndex == pageIndex && data.direction == 'down') {
            snapContainer.snap.removeClass('hide').addClass('show');
            $screen.find('.description').removeClass('hide').addClass('show');
        }

        if (data.nextIndex == pageIndex - 1) {
            stopAnimation();
            snapContainer.snap.removeClass('show').addClass('hide');
            $screen.find('.description').removeClass('show').addClass('hide');
        }

        if (data.nextIndex == pageIndex + 1) {
            stopAnimation();
        }
    });

    scrollListener.onStop((event, data) => {
        if (data.index == pageIndex) {
            startAnimation();
        }
    });
};

const SVGReadyHandler = () => {
    shapes = Snap(snapContainer.snap).selectAll('#shapes polygon, #shapes ellipse, #shapes triangle, #shapes rect, #shapes circle');
    gearsList = snapContainer.snap.selectAll('*[id^="gear-"]');
    gearsList.forEach(item => {
        gearsBBoxes.push(item.getBBox());
    });

    shapes.forEach(item => {
        shapesBBoxes.push(item.getBBox());
        shapesDestinations.push({
            x: _.random(-30, 30),
            y: _.random(20, 50)
        });
    });

    InitGearsAnimation();
};

// const GearsWheelEventHandler = (relativeScroll) => {
//     currentRotationAmount = maxRotationAmount * relativeScroll;

//     shapes.forEach((item, index) => {
//         const bBox = shapesBBoxes[index];
//         const destination = {
//             x: shapesDestinations[index].x * relativeScroll,
//             y: shapesDestinations[index].y * relativeScroll
//         }
//         if (index % 2 > 0) {
//             item.transform('t '+destination.x + ',' + destination.y + 'r'+ currentRotationAmount / 3+',' + bBox.cx +',' + bBox.cy);
//         } else {
//             item.transform('t '+destination.x + ',' + destination.y + 'r'+ (-currentRotationAmount / 3) + ',' + bBox.cx +',' + bBox.cy);
//         }
//     });
    
//     // SETTING ROTATE DIRECTION
//     gearsList.forEach((item, index) => {
//         const bBox = gearsBBoxes[index];
//         const id = item.node.id;
//         const isOdd = id[id.length - 1] % 2 > 0;
//         item.transform('r'+ currentRotationAmount * (isOdd ? -1 : 1)+',' + bBox.cx +',' + bBox.cy);
//     });
// };

const AnimateShape = (shape) => {
    const starBBox = shape.getBBox();
    const maxScale = 1.3;
    shape.animate({
        transform: 's '+maxScale+' '+maxScale+' '+starBBox.cx+' '+starBBox.cy,
        opacity: .8
    }, 300, mina.easeinout, () => {
        shape.animate({
            transform: 's .2 .2 '+starBBox.cx+' '+starBBox.cy,
            opacity: 0
        }, 300, mina.easeinout, () => {
            shape.remove();
        });
    });
};

const InitGearsAnimation = () => {
    
    let animationCallBack = () => {};
    let isAnimating = false;

    const animate = () => {
        if (isAnimating) return;
        isAnimating = true;
        Snap.animate(0, 1, step => {

            if (step * 1000 % 2 == 0) {
                const randIndex = _.random(0, shapes.length - 1);
                AnimateShape(shapes[randIndex].clone());
            }
            
            gearsList.forEach((item, index) => {
                const bBox = gearsBBoxes[index];
                const id = item.node.id;
                const isOdd = id[id.length - 1] % 2 > 0;
                item.transform('r'+ 360 * step * (isOdd ? -1 : 1)+',' + bBox.cx +',' + bBox.cy);
            });
        }, 2000, mina.linear, () => {
            isAnimating = false;
            animationCallBack();
        });
    };

    startAnimation = () => {
        animationCallBack = animate;
        animate();
    };

    stopAnimation = () => {
        animationCallBack = () => {};
    };
};