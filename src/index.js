require('prefixfree');
require('./scripts/polyfills/conicGradientPolyfill');
require('./scripts/polyfills/backgroundClipPolyfill');
require('./style/style.scss');
// require('smoothscroll-polyfill').polyfill();

//WALKAROUND TO UPLOAD ALL FILES
const getAllSVG = svgName => {
  return require('./media/svg/' + svgName);
};
const getAllImages = imageName => {
  return require('./media/' + imageName);
};

import ScrollListener from './scripts/scrollListener';
import FlashMessage from './scripts/flashMessage';
import {isMobile} from './scripts/tools';
import _ from 'underscore';

$(window).on('load', () => {
  setTimeout(() => {
    scrollListener.scrollToIndex(0);
    $('body').addClass('ready');
  }, 0);
});

$(document).ready(() => {
  window.scrollListener = new ScrollListener('#fullpage');
  LoadPageModules();
  SubscsribeOnEvents();
  tabsBox();
});

function tabsBox() {
  $('.tabs-box .tabs-item a').on('click', function (e) {
      e.preventDefault();
      var $parent = $('.tabs-box'),
          $target = $(this).closest('li').attr('data-id');

      $parent.find('li').removeClass('active');
      $(this).closest('li').addClass('active');
      $('.tabs-box .tabs-content').removeClass('active');
      $('.tabs-box .tabs-content').eq($target-1).addClass('active');
      $('.tabs-box .tabs-item').removeClass('open');
  })

    $('.tabs-box .menu-head').on('click', function (e) {
        $('.tabs-box .tabs-item').toggleClass('open');
    })

}


function LoadPageModules() {

  const path = window.location.pathname.replace(new RegExp('\/|\.html', 'g'), '');
  $('body').addClass(path.length ? path : 'index');
  $('body').addClass(isMobile.any() ? 'is-mobile' : '');
  require('./scripts/header').default();

  switch(path) {
    case '':
    case 'index': {
      $('body').addClass('show-base-back');
      require('./scripts/customers').default(1);
      // require('./scripts/text').default(2);
      require('./scripts/performance').default(2);
    } break;
    case 'product': {
      $('body').addClass('show-product-back');
      scrollListener.onMove((event, data) => {
        if ([1, 2].includes(data.nextIndex)) {
          $('header').addClass('dark');
        } else {
          $('header').removeClass('dark');
        }
      });
      // require('./scripts/text').default(1);
      require('./scripts/product').default(1);
      require('./scripts/integration').default(2);
    } break;
    case 'solution': {
      $('body').addClass('show-solution-back');
      scrollListener.onMove((event, data) => {
        if ([1, 3].includes(data.nextIndex)) {
          $('header').addClass('dark');
        } else {
          $('header').removeClass('dark');
        }
      });
      require('./scripts/flowScheme').default(1);
      require('./scripts/benefits').default(2);
    } break;
    case 'faq': {
      require('./scripts/faq').default();
    } break;
    case 'contacts': {
      require('./scripts/contacts').default();
    }
    default: break;
  }
};

function SubscsribeOnEvents() {

  scrollListener.onMove((event, data) => {
    if (data.nextIndex == 1 && data.direction == 'up') {
      $('body').addClass('show-head-back').addClass('show-base-back');
    }

    if (data.nextIndex == 2 && data.direction == 'down') {
      $('body').removeClass('show-head-back').removeClass('show-base-back');
    }
  })

  $('#footFormAnchor').on('click', () => {
    window.scrollListener.scrollToLast();
    $('body').removeClass('show-head-back').removeClass('show-base-back');
  });

  $('.down-pointer').on('click', () => {
    window.scrollListener.scrollNext();
  })

  $('#menuToggleButton').on('click', () => {
    $('#menuScreen').toggleClass('show');
    if ($('#menuScreen').hasClass('show'))  {
      scrollListener.scrollSwitcher.disableScroll();
    } else {
      scrollListener.scrollSwitcher.enableScroll();
    }
  });

  $(window).on('scroll', function(e) {
    if ($('#menuScreen').hasClass('show') && !scrollListener.isScrollDisabled)  {
      scrollListener.scrollSwitcher.disableScroll();
    } else if (scrollListener.isScrollDisabled && !scrollListener.eventsBlocked) {
      scrollListener.scrollSwitcher.enableScroll();
    }
  });

  $('.menu-screen-item').hover(
    //HOVER ON
    function(e) {
      const data = $(this).data();
      const itemName = data['hoveritem'];
      const $menuScreen = $('#menuScreen');

      $menuScreen.addClass(itemName);
      if (!$menuScreen.hasClass('before')) {
        $menuScreen.addClass('before');
        $menuScreen.removeClass('after');
      } else {
        $menuScreen.addClass('after');
        $menuScreen.removeClass('before');
      }
    //HOVER OUT
    }, function(e) {
      const data = $(this).data();
      const itemName = data['hoveritem'];
      $('#menuScreen').removeClass(itemName);
    }
  );

  //FORM SUBMITION
  $('#sendEmailForm, #contactsForm').on('submit', function(e) {
    e.preventDefault();
    const $form = $(this);
    $.ajax({
      url: 'https://mg.tryoup.io/send-email/' + $(this).data('method'),
      type: 'POST',
      data: $form.serialize(),
      crossDomain: true,
      success: function(data) {
        if (data.status == 'error') {
          _.each(_.keys(data.messages), (key) => {
            const message = data.messages[key][0];
            const $field = $form.find('.input-field.' + key);
            $field.addClass('error');
            $field.append('<span class="error">' + message + '</span>')
          });

          return;
        }
        new FlashMessage('success', 'Thanks! <br /> Your request has been sent. <br /> We’ll come back to you soon!');
        $form.find('.input-field input, .input-field textarea').val('');
      },
      error: function(error) {
        new FlashMessage('error', error['statusText']);
      }
    });
  });

  $('.input-field input, .input-field textarea').on('focus', function() {
    const $parent = $(this).parent('.input-field');
    $parent.removeClass('error');
    $parent.find('.error').remove();
  });
};