'use strict';

var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackLayoutPlugin = require('html-webpack-layout-plugin');
var FileWatcherPlugin = require("file-watcher-webpack-plugin");
var CompressionPlugin = require("compression-webpack-plugin");
var ImageminPlugin = require("imagemin-webpack-plugin").default;

module.exports = {
    entry: [
        'index.js'
    ],
    output: {
        path: path.join(__dirname, "build"),
        filename: '[name].[hash].js',
    },
    module: {
        loaders: [
            {
                test: require.resolve('snapsvg'),
                loader: 'imports-loader?this=>window,fix=>module.exports=0'
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "sass-loader"
                    }],
                    // use style-loader in development
                    fallback: "style-loader"
                })
            },
            {
                test: /(eot|ttf|svg|woff|woff2|otf)(\?[a-z0-9=.]+)?$/i,
                loader: 'file-loader?name=fonts/[name].[ext]',
                include: path.join(__dirname, 'src/font')
            },
            {
                test: /\.(jpe?g|png|gif)$/i,
                loader: 'file-loader?name=media/[name].[ext]&limit=8192',
                include: path.join(__dirname, 'src/media')
            },
            {
                test: /\.svg$/i,
                loader: 'file-loader?name=media/svg/[name].[ext]',
                include: path.join(__dirname, 'src/media/svg')
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                // Only run `.js` and `.jsx` files through Babel
                test: /\.js$/,
                loader: "babel-loader",
                // Skip any files outside of your project's `src` directory
                include: [
                    path.resolve(__dirname, "src"),
                ],
                // Options to configure babel with
                query: {
                    plugins: ['transform-runtime'],
                    presets: ['es2015'],
                }
            },
            { test: /jquery-mousewheel/, loader: "imports-loader?define=>false&this=>window" },
			{ test: /malihu-custom-scrollbar-plugin/, loader: "imports-loader?define=>false&this=>window" }
        ]
    },
    resolve: {
        modules: [
            path.join(__dirname, "src"),
            "node_modules"
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'style.[hash].css',
            allChunks: true,
            disable: false
        }),
        new HtmlWebpackPlugin({
            inject: true,
            layout: path.join(__dirname, 'src/templates/layout.html'),
            template: path.join(__dirname, 'src/templates/index.html'),
            filename: 'index.html'
        }),
        new HtmlWebpackPlugin({
            inject: true,
            layout: path.join(__dirname, 'src/templates/layout.html'),
            template: path.join(__dirname, 'src/templates/product.html'),
            filename: 'product.html'
        }),
        new HtmlWebpackPlugin({
            inject: true,
            layout: path.join(__dirname, 'src/templates/layout.html'),
            template: path.join(__dirname, 'src/templates/solution.html'),
            filename: 'solution.html'
        }),
        new HtmlWebpackPlugin({
            inject: true,
            layout: path.join(__dirname, 'src/templates/layout.html'),
            template: path.join(__dirname, 'src/templates/faq.html'),
            filename: 'faq.html'
        }),
        new HtmlWebpackPlugin({
            inject: true,
            layout: path.join(__dirname, 'src/templates/contacts-layout.html'),
            template: path.join(__dirname, 'src/templates/contacts.html'),
            filename: 'contacts.html'
        }),
        new HtmlWebpackLayoutPlugin(),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),

        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            comments: false,
            compress: {
                sequences: true,
                booleans: true,
                loops: true,
                unused: true,
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        }),
        new ImageminPlugin({
            disable: process.env.NODE_ENV !== 'production',
            pngquant: {
                quality: '70-100'
            },
            svgo: null
        })
    ]
};